# Instructions to create a security pipeline in GitLab for a MAVEN project

## Create ssh key for repository

> See detailed instructions at:
**https://docs.gitlab.com/ee/ssh/#rsa-ssh-keys**

```
cd .ssh/
ssh-keygen -t ed25519 -C "[comment]"
xclip -sel clip < ~/.ssh/id_ed25519.pub
```
---


## SonarCloud

## Define the SonarCloud Token environment variable

> 1. In GitLab, go to Settings > CI/CD > Variables to add the following variable and make sure it is available for your project:
In the Key field, enter SONAR_TOKEN 
In the Value field, enter 2c1eb06f498fe476ad2e1f5fa6001ace860dbc14 
Make sure that the Protect variable checkbox is unticked
Make sure that the Mask variable checkbox is ticked

> 2. Define the SonarCloud URL environment variable
Still in Settings > CI/CD > Variables add a new variable and make sure it is available for your project:
In the Key field, enter SONAR_HOST_URL 
In the Value field, enter https://sonarcloud.io 
Make sure that the Protect variable checkbox is unticked
No need to tick the Mask variable checkbox this time


# YAML file with pipeline instructions:

```
stages:
  - build
  - dependencycheck

build:
  image: maven
  stage: build
  script: mvn package
  artifacts:
    paths:
      - target/devsec-app-0.0.1-SNAPSHOT.jar

dependencycheck:
    stage: dependencycheck
    image:
        name: owasp/dependency-check
        entrypoint: [""]

    script:
        - /usr/share/dependency-check/bin/dependency-check.sh --project devsec-app --out . --scan . --enableExperimental 
```
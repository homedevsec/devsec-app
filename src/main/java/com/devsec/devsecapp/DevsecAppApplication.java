package com.devsec.devsecapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevsecAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevsecAppApplication.class, args);
	}

}

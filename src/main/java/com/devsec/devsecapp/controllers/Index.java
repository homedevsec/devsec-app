package com.devsec.devsecapp.controllers;

import java.security.SecureRandom;
import java.util.Random;

import com.devsec.devsecapp.models.Credential;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Index {

	@GetMapping("/")
	public String index(Credential form) {
		return "index";
	}

	@PostMapping("/login")
	public String login(@Validated Credential credential, BindingResult result, Model model) {

		if (result.hasErrors()) {
			return "index";
		}
		String userid = credential.getUserId();

		Random r= new SecureRandom();
		// mooie code
		int sessionId=r.nextInt(4000000);
		System.out.println("SessionId: " + sessionId);

		//decomment the following line to fix the xss vulnerability
		//userid = HtmlUtils.htmlEscape(userid);
		
		model.addAttribute("userid",userid);

		return "thanks";
	}

}

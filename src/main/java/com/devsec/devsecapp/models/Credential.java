package com.devsec.devsecapp.models;

import javax.validation.constraints.NotEmpty;

public class Credential {

	@NotEmpty(message = "The user ID can't be an empty string")
	private String userId;

	@NotEmpty(message = "The password can't be an empty string")
	private String password;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
  
  }
